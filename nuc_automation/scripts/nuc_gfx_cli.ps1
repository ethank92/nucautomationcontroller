## Windows NUC Graphics CLI
## Usage: ./nuc_gfx_cli -resolution <WxH> -fps <60, 59.94, 50, 30, 29.97, 25, 24, 23.97> -color_format <YCbCr, RGB> -quant_range <Full, Limited>
## By Ethan Klein, QSC Boulder 2022

param (
    [Parameter(Mandatory=$true)]$resolution,
    [Parameter(Mandatory=$true)]$fps,
    [Parameter(Mandatory=$false)]$color_format,
    [Parameter(Mandatory=$false)]$quant_range
)

$HKEY_LOCAL_MACHINE = 2147483650
$strKeyPath = "SYSTEM\CurrentControlSet\Control\GraphicsDrivers\Configuration"

$objReg = [WMIClass]"root\default:StdRegProv";

$arrSubKeys = $objReg.EnumKey($HKEY_LOCAL_MACHINE, $strKeyPath)

Get-CimInstance -ClassName CIM_VideoControllerResolution | Select-Object -Property ScanMode, Caption | Where-Object {$ScanMode -eq ""}

$Formats = Get-CimInstance -ClassName CIM_VideoControllerResolution | Select-Object -Property HorizontalResolution, VerticalResolution, RefreshRate | Where-Object {$_.Caption -NotMatch "Interlaced"} | Sort-Object -Descending -Property RefreshRate | Sort-Object -Descending -Property HorizontalResolution

foreach ($format in $Formats){
    #Write-Host $format.HorizontalResolution "x" $format.VerticalResolution "@" $format.RefreshRate "Hz"
}

function parseFormat ([string[]]$line){
    $tbl = ""
    $tbl = $line -split 'x'
    $tb2 = $tbl[0]
    $tb3 = $tbl[1]
    return $tb2, $tb3
}

$supported_res = @(
    '3840x2160'
    '2560x1600'
    '2560x1440'
    '1920x1200'
    '1920x1080'
    '1600x900'
    '1440x900'
    '1280x1024'
    '1280x800'
    '1280x720'
    '1024x768'
    '800x600'
    '640x480'
)

$res_unsupported = $true
foreach ($_ in $supported_res){
    #Write-Host $_ $resolution
    if ($_ -eq $resolution){
        $width, $height = parseFormat($resolution)
        $res_unsupported = $false
        break
    }
}

if (!$color_format){
    $color_format = "RGB"
}

if ($color_format -eq "YCbCr" -or $color_format -eq "RGB"){

} else {
    Write-Host "ERROR: Color format not supported" -ForegroundColor Red
    Write-Host "Please choose one of the following: RGB or YCbCr"
    break
}

if ($quant_range -eq "Full" -or $quant_range -eq "Limited"){

} else {
    Write-Host "ERROR: Quanitzaion range format not supported" -ForegroundColor Red
    Write-Host "Please choose one of the following: Full or Limited"
    break
}

if ($res_unsupported -eq $true){
    Write-Host "ERROR: Resolution not supported" -ForegroundColor Red
    Write-Host "Please choose one of the following:"
    foreach ($_ in $supported_res){
        Write-Host $_
    }
    Write-Host ""
    exit
}

if ($fps -eq "60"){
    $vsync_denom = 2475000
    $vsync_num = 148500000
}elseif ($fps -eq "50" ){
    $vsync_denom = 11880000
    $vsync_num = 594000000
}elseif ($fps -eq "59.94"){
    $vsync_denom = 2475000
    $vsync_num = 148351648
}elseif ($fps -eq "30"){
    $vsync_denom = 2475000
    $vsync_num = 74250000
}elseif ($fps -eq "29.97"){
    $vsync_denom = 2475000
    $vsync_num = 74175824
}elseif ($fps -eq "25"){
    $vsync_denom = 2970000
    $vsync_num = 74250000
}elseif ($fps -eq "24"){
    $vsync_denom = 3093750
    $vsync_num = 74250000
}elseif ($fps -eq "23.97"){
    $vsync_denom = 3093750
    $vsync_num = 74175824
}else{
    Write-Host "ERROR: Framerate not supported" -ForegroundColor Red
    Write-Host "Choose one of the following:"
    Write-Host "60, 59.94, 50, 30, 29.97, 25, 24, 23.96"
    Write-Host ""
    exit
}

$DisplayAdapterGUID = $(Get-PnpDeviceProperty -InstanceId $(Get-PnpDevice | Where-Object {$_.Status -match "OK"} | Where-Object {$_.Class -match "Display"}).InstanceId | Where-Object {$_.KeyName -match "DEVPKEY_Device_ClassGuid"}).Data

Write-Host "Setting Format: $resolution @ $fps $color_format"

foreach ($subKey in ($arrSubKeys.sNames)){
    #Write-Host $subKey
    if ($subKey -like "*QSC*"){
        $RegistryPath1 = "HKLM:\$strKeyPath\$subkey\00\00\" #secondary intel set reg
        $RegistryPath2 = "HKLM:\$strKeyPath\$subkey\00\" #primary intel set reg
        $RegistryPath3 = "HKLM:\SYSTEM\ControlSet001\Control\Class\$DisplayAdapterGUID\0000\" #primary display adapater set reg
        if ($resolution -eq "3840x2160"){
            Set-ItemProperty -Path $RegistryPath1 -Name "Scaling" -Value 2
        }else {
            Set-ItemProperty -Path $RegistryPath1 -Name "Scaling" -Value 3  
        }
        Set-ItemProperty -Path $RegistryPath2 -Name "PrimSurfSize.cx" -Value "$width"
        Set-ItemProperty -Path $RegistryPath2 -Name "PrimSurfSize.cy" -Value "$height"
        Set-ItemProperty -Path $RegistryPath1 -Name "ActiveSize.cx" -Value "$width"
        Set-ItemProperty -Path $RegistryPath1 -Name "ActiveSize.cy" -Value "$height"
        Set-ItemProperty -Path $RegistryPath1 -Name "PrimSurfSize.cx" -Value "$width"
        Set-ItemProperty -Path $RegistryPath1 -Name "PrimSurfSize.cy" -Value "$height"
        Set-ItemProperty -Path $RegistryPath1 -Name "VSyncFreq.Denominator" -Value "$vsync_denom"
        Set-ItemProperty -Path $RegistryPath1 -Name "VSyncFreq.Numerator" -Value "$vsync_num"
        if ($color_format -eq "RGB"){
            Set-ItemProperty -Path $RegistryPath3 -Name "MP_YCbCr_30E03" -Value 0
        }elseif ($color_format -eq "YCbCr") {
            Set-ItemProperty -Path $RegistryPath3 -Name "MP_YCbCr_30E03" -Value 1
        }
        if ($quant_range -eq "Full") {
            Set-ItemProperty -Path $RegistryPath3 -Name "MP_QuantRange_30E03" -Value 2
        }elseif ($quant_range -eq "Limited"){
            Set-ItemProperty -Path $RegistryPath3 -Name "MP_QuantRange_30E03" -Value 1
        }
    }
}


pnputil.exe /restart-device $(Get-WmiObject win32_videocontroller).PNPDeviceID | Out-Null