StatusLEDColors = {"limegreen","orange","red","gray","red","blue"}

--TIMERS
watcherrorTimer = Timer.New()
pollsessionTimer = Timer.New()
sessiontimeoutTimer = Timer.New()

Controls["Connect"].Value = 0
Controls["Connected"].Value = 0

rootpath = "C:\\nuc_automation"

--SSH OBJECTS
ssh = Ssh.New()

--VARIABLES
state_count = 0
tout_count = 0
try_count = 0
sshConnected = false
error_code = nil
timeout = 10

--STATUS AND LOGGING FUNCTIONS
function statusOK() --update status OK
  updateStatus("", "0")
  print("ready")
  stopwatchError()
end

function updateStatus(msg, state)
  Controls["Status"].Value = state
  Controls["Status"].String = msg
end

--GENERIC FUNCITONS
function string.starts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end

function split(inputstr, sep) --make table based on delimiter
  if sep == nil then
    sep = "%s"
  end
  local t={}
  if inputstr then
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
      table.insert(t, str)
    end
    return t
  end
end

function ignoreLineContains(line, pattern) --ignore line if line contains pattern
for w in string.gmatch (line, pattern) do
  ignore = line
end
  return ignore
end

function sizeof(T) --return size of table
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

--SCRIPT SPECIFIC FUNCTIONS

function pollSessionId()
  ssh:Write('qwinsta '.."SQA"..'\n')
end

function parseSessionId(line)
outgoing_path = ignoreLineContains(line, "C:\\")
env_line = ignoreLineContains(line, "SESSIONNAME")
micro_line = ignoreLineContains(line, "Microsoft")

--print(line)
if line ~= env_line and line ~= outgoing and line ~= micro_line then
  for w in string.gmatch(line, "Active") do
    sessionId = split(line)[3]
    print("Session ID: "..sessionId)
    tout_count = 0
  end
end
end

function checkReqsInstalled() --check if files are in correct spot
  cmd = string.format('if EXIST %s\\scripts\\nuc_gfx_cli.ps1 (echo reqs_installed error: 0) else (echo reqs_installed error: 1)\n', rootpath)
  ssh:Write(cmd)
  print(cmd)
  cmd_id = "reqs_installed"
  Timer.CallAfter(readErrorCode,1)
end

function stopPowershell() --stop all instances of powershell
  ssh:Write('powershell Stop-Process -Name "powershell"\n')
  requestErrorCode("stop_powershell")
end

function prepareWindowsEnv()
  cmd = string.format('psexec -i %s -s powershell "%s\\prepare_env.ps1"\n', rootpath, sessionId)
  ssh:Write(cmd)
  print(cmd)
  requestErrorCode("prepare_env")
end

function changeFormat()
  video_format = split(Controls["Video Format"].String, "p")
  resolution = video_format[1]
  fps = video_format[2]
  quant = Controls["Quantization Range"].String
  color_format = Controls["Color Format"].String
  cmd = string.format('powershell %s\\scripts\\nuc_gfx_cli.ps1 -resolution %s -fps %s -color_format %s -quant_range %s\n', rootpath, resolution, fps, color_format, quant)
  requestErrorCode("change_format")
  updateStatus(string.format("Changing Format: %s %s %s", Controls["Video Format"].String, color_format, quant), 0)
  Timer.CallAfter(function() updateStatus("",0) end, 3)
  ssh:Write(cmd)
  print(cmd)
end

--ERROR CODE FUNCTIONS

function stopwatchError()
  error_code = nil
  cmd_id = ""
  watcherrorTimer:Stop()
  print("stopwatchError")
end

function requestErrorCode(script_name) --ask ssh session what last error code was and tag it unique script name
  ssh:Write('echo '..script_name..' error: %errorlevel%\n')
  cmd_id = script_name
  Timer.CallAfter(readErrorCode,.5)
end

function parseErrorCode(line) --parse error code from ssh buffer based on tagged script name
  outgoing_path = ignoreLineContains(line, "C:\\")
  env_line = ignoreLineContains(line, "errorlevel")
  if line ~= outgoing_path and line ~= env_line then
  --print(line)
    for w in string.gmatch(line, "reqs_installed") do
      cmd_id = split(line, " ")[1]
      error_code = tonumber(split(line, " ")[3])
      print(line)
    end
    for w in string.gmatch(line, "prepare_env") do
      script = split(line, " ")[1]
      error_code = tonumber(split(line, " ")[3])
      print(line)
    end
    for w in string.gmatch(line, "change_format") do
      script = split(line, " ")[1]
      error_code = tonumber(split(line, " ")[3])
      print(line)
    end
  end
  --print(line)
end


function watchError() --watch ssh buffer for error codes and do things based on code
print("**")
watch_count = watch_count + 1
if watch_count == 2 then
  watcherrorTimer:Stop()
  updateStatus(script.." failed", 2)
  Timer.CallAfter(statusOK, 2)
end
if error_code ~= nil then
  if cmd_id == "reqs_installed" then
    if error_code == 0 then
      stopPowershell()
    else
      updateStatus("One or more files not found - check that windows files are in the correct directory", 2)
      stopSession()
    end
  end
  if cmd_id == "stop_powershell" then
    if error_code == 0 or error_code == -1 then
      prepareWindowsEnv()
      updateStatus("stopped powershell", 5)
    else
      prepareWindowsEnv()
      updateStatus("no powershell instances", 5)
    end
  end
  if cmd_id == "prepare_env" then
    if error_code ~= 1 then
      updateStatus("preparing windows environment", 5)
      Timer.CallAfter(statusOK, 2)
    else
      updateStatus("preparing windows environment failed", 2)
      stopSession()
    end
  end
    stopwatchError()
  end
end

function readErrorCode() --read error code form ssh parse using different timer because while loops are bad
  watch_count = 0
  watcherrorTimer:Start(1)
end

--MAIN SSH
ssh.Connected = function()
  Controls["Connected"].Value = 1
  sshConnected = true
  print("ssh connected")
  pollSessionId()
  checkReqsInstalled()
end

ssh.Data = function()
  is_parsed = false
  line = ssh:ReadLine(TcpSocket.EOL.Any)
  while line do
    parseErrorCode(line)
    parseSessionId(line)
    --print(line)
    line = ssh:ReadLine(TcpSocket.EOL.Any)
  end
end

ssh.Reconnect = function()
  --Controls["Connected"].Value = 0
  print("ssh reconnect")
  updateStatus("SSH reconnecting", 5)
end

ssh.Closed = function()
  Controls["Connected"].Value = 0
  print("ssh closed")
  --stopSession()
  sshConnected = false
end

ssh.Error = function(s, err)
  Controls["Connected"].Value = 0
  print("ssh error", err)
  ssh:Disconnect()
  updateStatus("", 4)
end

ssh.Timeout = function()
  Controls["Connected"].Value = 0
  print("ssh timeout")
  updateStatus("SSH timed out - check server is running on Windows PC", 2)
end

ssh.LoginFailed = function()
  Controls["Connected"].Value = 0
  print("ssh LoginFailed")
  updateStatus("SSH login failed - check server is running on Windows PC", 2)
end

function enableClient(ctl)
if ctl.Value == 1 then
  address = Controls["Host"].String
  port = 22
  user = "SQA"
  password = "qsc123456!!!"
  ssh:Connect(address, port, user, password)
else
  ssh:Disconnect()
  updateStatus("Not Connected", 3)
  watcherrorTimer:Stop()
end
end

function stopSession()
  tout_count = 0
  watcherrorTimer:Stop()
  sessiontimeoutTimer:Stop()
  pollsessionTimer:Stop()
  Controls["Connect"].Value = 0
  table_dropdown_formated = {}
  pc_formatChoices = table_dropdown_formated
  pc_formatString = "No Formats"
end

updateStatus("Not Connected", 3)

address = Controls["Host"].String
port = 22
user = "SQA"
password = "qsc123456!!!"
ssh:Connect(address, port, user, password)

pollsessionTimer.EventHandler = pollSessionId
watcherrorTimer.EventHandler = watchError
Controls["Connect"].EventHandler = enableClient
Controls["Video Format"].Choices = {
  '3840x2160p60',
  '2560x1600p60',
  '2560x1440p60',
  '1920x1200p60',
  '1920x1080p60',
  '1600x900p60',
  '1440x900p60',
  '1280x1024p60',
  '1280x800p60',
  '1280x720p60',
  '1024x768p60',
  '800x600p60',
  '640x480p60'
  }
Controls["Color Format"].Choices = {
  "RGB",
  "YCbCr"
}
Controls["Color Format"].String = "RGB"
Controls["Quantization Range"].Choices = {
  "Full",
  "Limited"
}
Controls["Quantization Range"].String = "Full"
Controls["Video Format"].EventHandler = changeFormat
Controls["Color Format"].EventHandler = changeFormat
Controls["Quantization Range"].EventHandler = changeFormat