  --GROUP BOXES
  table.insert(graphics,{Type = "GroupBox",  Text = "Status",          HTextAlign = "Left", Position = {0,0},   Size = {240,60},   CornerRadius = 8, StrokeWidth = 1,  StrokeColor = {0,0,0}})
  table.insert(graphics,{Type = "GroupBox",  Text = "Connection Info", HTextAlign = "Left", Position = {0,64},  Size = {240,112},  CornerRadius = 8, StrokeWidth = 1,  StrokeColor = {0,0,0}})
  table.insert(graphics,{Type = "GroupBox",  Text = "Control",         HTextAlign = "Left", Position = {0,184}, Size = {240,100},  CornerRadius = 8, StrokeWidth = 1,  StrokeColor = {0,0,0}})

  table.insert(graphics,{Type = "Label",     Text = "IP Address",      HTextAlign = "Right",  Position = {10,98},    Size = {68,16}})
  table.insert(graphics,{Type = "Label",     Text = "Video Format",      HTextAlign = "Right",  Position = {10,220},    Size = {72,16}})
  table.insert(graphics,{Type = "Label",     Text = "Color Format",      HTextAlign = "Right",  Position = {10,240},    Size = {72,16}})
  table.insert(graphics,{Type = "Label",     Text = "Quantization",      HTextAlign = "Right",  Position = {10,260},    Size = {72,16}})
  table.insert(graphics,{Type = "Label",     Text = "Connect",         HTextAlign = "Right",  Position = {110,152},  Size = {68,16}})

  layout["Connected"] =  {PrettyName="Status~LED",                            Style = "Indicator",  IndicatorType = "LED",              Position = {8,30},     Size = {16,16}}
  layout["Status"] =     {PrettyName="Status~Status",                         Style = "Indicator",  TextBoxStyle = "Normal",            Position = {26,22},    Size = {200,32}}

  layout["Host"] = {PrettyName="Status~Connection Info~IP Address",     Style = "Text",       TextBoxStyle = "Normal",            Position = {80,98},    Size = {140,16}}
  layout["Connect"] =    {PrettyName="Status~Connection Info~Connect",        Style = "Button",     ButtonStyle = "Toggle",             Position = {184,152},  Size = {36,16}}
  layout["Video Format"] = {Style = "ComboBox",   TextBoxStyle = "Normal",  Position = {84,220},  Size = {124,16}}
  layout["Color Format"] = {Style = "ComboBox",   TextBoxStyle = "Normal",  Position = {84,240},  Size = {124,16}}
  layout["Quantization Range"] = {Style = "ComboBox",   TextBoxStyle = "Normal",  Position = {84,260},  Size = {124,16}}