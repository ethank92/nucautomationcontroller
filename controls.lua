table.insert(ctrls, {
  Name = "Connected",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "Connect",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "Status",
  ControlType = "Indicator",
  IndicatorType = "Status",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "Host",
  ControlType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "Username",
  ControlType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls,{
  Name = "Video Format", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Quantization Range", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "Color Format", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})


